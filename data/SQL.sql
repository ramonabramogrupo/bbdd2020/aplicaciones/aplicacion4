﻿DROP DATABASE IF EXISTS aplicacion3;
CREATE DATABASE aplicacion3;
USE aplicacion3;

CREATE TABLE coches(
  id int,
  marca varchar(50),
  fecha date,
  precio float,
  PRIMARY KEY (id)
  );

CREATE TABLE clientes(
  cod int,
  nombre varchar(50),
  idCocheAlquilado int,
  fechaAlquiler date,
  PRIMARY KEY (cod),
  UNIQUE KEY (idCocheAlquilado)
  );

ALTER TABLE clientes ADD CONSTRAINT fkClientesCoches 
  FOREIGN KEY (idCocheAlquilado) REFERENCES coches(id) 
  ON DELETE CASCADE ON UPDATE CASCADE;
